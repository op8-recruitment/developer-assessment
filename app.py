import crypt as OP8
import environment as env
import requests as r
import json
from pprint import pprint


data = r.get(env._return())
print(f"Requesting: {env._return()}")

if data.status_code == 200:
    data = json.loads(data)
    pprint(OP8.decrypt(json.loads(data['payload']),data['key']))
elif data.status_code == 404:
    print('Something didn\'t work right...')
