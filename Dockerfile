FROM python:3
ADD requirements.txt .
RUN pip install -r requirements.txt
COPY . .
ENV TRIM_STR="aHR0cHM6Ly9wYXN0ZWJpbi5jb20vcmF3L0hnakVOMXNwCg=="
CMD [ "python", "./app.py"]
