from cryptography.fernet import Fernet


def encode(string):
    key = Fernet.generate_key()
    cipher_suite = Fernet(key)
    return { "key": key, "payload": cipher_suite.encrypt(string.encode()) }

def decode(string, key):
    cipher_suite = Fernet(key)
    return cipher_suite.decrypt(string)
